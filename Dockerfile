FROM debian/eol:wheezy
# FROM debian/eol:jessie

USER 0
#ARG user=jenkins
#ARG group=jenkins
#ARG uid=1000
#ARG gid=1000

#RUN groupadd -g "${gid}" "${group}" \
#  && useradd -l -c "Jenkins user" -d /home/${user} -u "${uid}" -g "${gid}" -m "${user}"

#ARG AGENT_WORKDIR=/home/${user}/agent
ARG AGENT_WORKDIR=/home/jenkins/agent
ENV TZ=Etc/UTC

RUN cat /etc/apt/sources.list

RUN apt-get update \
  && apt-get --yes install \
    ca-certificates \
    curl \
    fontconfig \
    git \
    less \
    netbase \
    openssh-client \
    patch \
    tzdata \
    apt \
    apt-utils \
    sudo \
    apt-utils \
    aufs-tools \
    automake \
    build-essential \
    chrpath \
    cpio \
    dpkg-dev \
    diffstat \
    dosfstools \
    fakeroot \
    gawk \
    gcc \
    gcc-multilib \
    libcurl4-openssl-dev \
    libsdl1.2-dev \
    lzop \
    make \
    mtools \
    parted \
    patch \
    python \
    socat \
    subversion \
    syslinux \
    texinfo \
    tree \
    unzip \
    wget \
    zip \
    lib32z1 \ 
    lib32z1-dev \
  && apt-get clean \
  && rm -rf /tmp/* /var/cache/* /usr/share/doc/* /usr/share/man/* /var/lib/apt/lists/*

# install jenkins agent
ARG VERSION=3107.v665000b_51092
# ADD --chown="${user}":"${group}" "https://repo.jenkins-ci.org/public/org/jenkins-ci/main/remoting/${VERSION}/remoting-${VERSION}.jar" /usr/share/jenkins/agent.jar
ADD "https://repo.jenkins-ci.org/public/org/jenkins-ci/main/remoting/${VERSION}/remoting-${VERSION}.jar" /usr/share/jenkins/agent.jar
RUN chmod 0644 /usr/share/jenkins/agent.jar \
  && ln -sf /usr/share/jenkins/agent.jar /usr/share/jenkins/slave.jar

ENV LANG C.UTF-8

#Install opendjk 11
RUN curl -O https://download.java.net/java/GA/jdk11/9/GPL/openjdk-11.0.2_linux-x64_bin.tar.gz \
    && tar -zxvf openjdk-11.0.2_linux-x64_bin.tar.gz \
    && mv jdk-11* /usr/local/ \
    && rm openjdk-11.0.2_linux-x64_bin.tar.gz
ENV JAVA_HOME=/usr/local/jdk-11.0.2
ENV PATH=$PATH:$JAVA_HOME/bin

#install java 6 (not yet sure if needed)
RUN curl -O https://cdn.azul.com/zulu/bin/zulu6.22.0.3-jdk6.0.119-linux_x64.tar.gz && \
    tar -zxvf zulu6.22.0.3-jdk6.0.119-linux_x64.tar.gz && \
    mv zulu6.22.0.3-jdk6.0.119-linux_x64 /usr/local/ && \
    rm zulu6.22.0.3-jdk6.0.119-linux_x64.tar.gz
#ENV JAVA_HOME=/usr/local/zulu6.22.0.3-jdk6.0.119-linux_x64
#ENV PATH=$PATH:$JAVA_HOME/bin

# Installing the Cross-compiler from ELDK 4.2
COPY eldk-4.2/ data/eldk/eldk-4.2
RUN chmod +x data/eldk/eldk-4.2/eldk_init

# Be Jenkins
# USER "${user}"
ENV AGENT_WORKDIR="${AGENT_WORKDIR}"
# RUN mkdir /home/${user}/.jenkins && mkdir -p "${AGENT_WORKDIR}"
RUN mkdir /home/jenkins && mkdir /home/jenkins/.jenkins && mkdir -p "${AGENT_WORKDIR}"


# Create Jenkins Homedir
# VOLUME /home/"${user}"/.jenkins
VOLUME /home/jenkins/.jenkins
VOLUME "${AGENT_WORKDIR}"
# WORKDIR /home/"${user}"
WORKDIR /home/jenkins